 // Action Types
export enum ActionTypes {
  HELLO = 'HELLO',
  GOODBYE = 'GOODBYE',
}

 // Action Interfaces
export type Action = {
  type: ActionTypes.HELLO,
  payload: {
    name: string,
  },
} | {
  type: ActionTypes.GOODBYE,
};

// Action Creators
export const ActionCreators = {
  hello: (name: string): Action => ({
    type: ActionTypes.HELLO,
    payload: { name },
  }),
  goodbye: (): Action => ({
    type: ActionTypes.GOODBYE,
  }),
};
